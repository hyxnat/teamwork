Please read the below solution assumptions before analyzing it.

  * The problem statement explicitly states that output has to be a sorted list of domain names with occurrences altogther
  * Reading has been done using a buffer line by line and should handle large files - tested with a file containing 1.15M rows(large_file.csv in importer/fixtures)
  * Have have assumed that the possible number of uniq domain names will not be as huge as the input, since they are domain names. Sorting of final results is therefore done in memory
  * Have handled basic errors just for showcasing error handling

Gom is used as the package manager.

Use the below command for gom

```
go get github.com/mattn/gom
```

Use the below command to get dependent packages for running the code. This will create a vendor folder with dependencies

```
gom install
```

Use the below command for running the code.

```
go run main.go
```

Use the below command to run tests - Gingko and Gomega have been used for testing.

```
gom exec ginkgo -skipPackage vendor --randomizeAllSpecs --randomizeSuites -r
```
