package importer

const headerText string = "first_name,last_name,email,gender,ip_address"

type importer struct {
	domainNames map[string]int
	path        string
	err         error
}

// NewImporter - Import processor, reads , sort and logs errors
func NewImporter(path string) *importer {
	return &importer{path: path, domainNames: map[string]int{}}
}

func (r *importer) Execute() ([][]string, error) {
	errorChannel := make(chan error)
	domainChannel := make(chan string)
	done := make(chan bool)

	go r.read(domainChannel, errorChannel, done)
	go r.write(domainChannel)
	go r.logError(errorChannel)

	<-done

	if r.err != nil {
		return [][]string{}, r.err
	}

	return r.sort(), nil
}
