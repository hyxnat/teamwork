package importer_test

import (
	"./"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Importer", func() {
	It("finds domain names and corresponding count", func() {
		imp := importer.NewImporter("fixtures/sample.csv")
		values, err := imp.Execute()

		Expect(err).NotTo(HaveOccurred())
		Expect(values[0][0]).Should(Equal("gameofthrones.com"))
		Expect(values[0][1]).Should(Equal("33"))
		Expect(values[1][0]).Should(Equal("gmail.com"))
		Expect(values[1][1]).Should(Equal("33"))
		Expect(values[2][0]).Should(Equal("terminator.com"))
		Expect(values[2][1]).Should(Equal("33"))
		Expect(values).Should(HaveLen(3))
	})

	It("throws improper format error", func() {
		imp := importer.NewImporter("fixtures/sample2.csv")
		values, err := imp.Execute()

		Expect(err.Error()).To(Equal("Improper file format"))
		Expect(values).Should(HaveLen(0))
	})
})
