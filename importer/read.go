package importer

import (
	"bufio"
	"errors"
	"os"
	"path/filepath"
)

func (r *importer) read(domainChannel chan<- string, errorChannel chan<- error, done chan<- bool) {
	absPath, _ := filepath.Abs(r.path)
	f, err := os.Open(absPath)

	defer close(domainChannel)
	defer close(errorChannel)
	defer f.Close()

	if err != nil {
		done <- true
		return
	}

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	headers := scanner.Text()
	if headers != headerText {
		errorChannel <- errors.New("Improper file format")
		done <- true
		return
	}

	for scanner.Scan() {
		domainChannel <- scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		errorChannel <- err
	}
	done <- true
}
