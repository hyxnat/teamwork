package importer

import (
	"sort"
	"strconv"
)

// Sort - in memory operation assuming possible output is not too large
func (r *importer) sort() [][]string {
	var keys []string
	var result [][]string

	for k := range r.domainNames {
		keys = append(keys, k)
	}

	sort.Strings(keys)
	for _, n := range keys {
		val := strconv.Itoa(r.domainNames[n])
		result = append(result, []string{n, val})
	}

	return result
}
