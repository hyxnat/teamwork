package importer

import "strings"

// Write - appends value to a map of domain names with corresponding count, handles basic errors
func (r *importer) write(domainChannel <-chan string) {
	for line := range domainChannel {
		email := strings.Split(line, ",")
		if len(email) < 3 {
			continue
		}
		emailStr := email[2]
		ar := strings.Split(emailStr, "@")
		if len(ar) < 2 {
			continue
		}
		domainName := ar[len(ar)-1]

		if val, ok := r.domainNames[domainName]; ok {
			r.domainNames[domainName] = val + 1
		} else {
			r.domainNames[domainName] = 1
		}
	}
}

func (r *importer) logError(errChannel <-chan error) {
	for err := range errChannel {
		r.err = err
	}
}
