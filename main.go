package main

import (
	"fmt"

	"./importer"
)

func main() {
	imp := importer.NewImporter("importer/fixtures/customers.csv")
	values, err := imp.Execute()
	if err != nil {
		fmt.Print(err.Error())
	} else {
		fmt.Print(values)
	}
}
